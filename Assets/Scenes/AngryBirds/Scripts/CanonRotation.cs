using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//Roger Ribas
public class CanonRotation : MonoBehaviour
{
    public Vector3 _maxRotation;
    public Vector3 _minRotation;
    private float offset = -51.6f;
    public GameObject ShootPoint;
    public GameObject Bullet;
    public float MaxSpeed;
    public float MinSpeed;
    public GameObject PotencyBar;
    private float initialScaleX;
    public Camera mainCam;

    private float ProjectileSpeed = 0f;

    private void Awake()
    {
        initialScaleX = PotencyBar.transform.localScale.x;
    }

    void Update()
    {
        Vector3 mousePos = Input.mousePosition;
        mousePos = mainCam.ScreenToWorldPoint(mousePos);
        var dist = mousePos - ShootPoint.transform.position;

        //Uso Rad2Deg porque mola mas que 180/Mathf.PI >:)
        var ang = Mathf.Atan2(dist.y, dist.x) * Mathf.Rad2Deg + offset;
        transform.rotation = Quaternion.Euler(0, 0, ang);

        if (Input.GetMouseButton(0))
        {
            ProjectileSpeed += Time.deltaTime * 4f; // Incrementa la velocidad cada frame
        }

        if (Input.GetMouseButtonUp(0))
        {
            var projectile = Instantiate(Bullet, ShootPoint.transform.position, Quaternion.identity);
            projectile.GetComponent<Rigidbody2D>().velocity = new Vector2(dist.x, dist.y).normalized * ProjectileSpeed;
            ProjectileSpeed = 0f;
        }

        CalculateBarScale();
    }

    public void CalculateBarScale()
    {
        //Barra truco
        PotencyBar.transform.localScale = new Vector3(Mathf.Lerp(0, initialScaleX, ProjectileSpeed / MaxSpeed),
        PotencyBar.transform.localScale.y,PotencyBar.transform.localScale.z);
    }
}
